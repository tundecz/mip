package model;

public abstract class Mamifer{
	
	private String name;
	private int age;
	private String race;
	private String owner;
	private int weight;
	
	public Mamifer(String name, int age, String race, String owner, int weight) {
		this.name = name;
		this.age = age;
		this.race = race;
		this.owner = owner;
		this.weight = weight;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public String getRace() {
		return this.race;
	}
	
	public String getOwner() {
		return this.owner;
	}
	
	public int getWeight() {
		return this.weight;
	}
}

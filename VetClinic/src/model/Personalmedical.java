package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPersonalMedical;

	private String name;

	//bi-directional many-to-one association to Animal
	@OneToMany(mappedBy="personalmedical")
	private Set<Animal> animals;

	//bi-directional many-to-one association to Diagnostic
	@OneToMany(mappedBy="personalmedical")
	private Set<Diagnostic> diagnostics;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="personalmedical")
	private Set<Programare> programares;

	public Personalmedical() {
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Animal> getAnimals() {
		return this.animals;
	}

	public void setAnimals(Set<Animal> animals) {
		this.animals = animals;
	}

	public Animal addAnimal(Animal animal) {
		getAnimals().add(animal);
		animal.setPersonalmedical(this);

		return animal;
	}

	public Animal removeAnimal(Animal animal) {
		getAnimals().remove(animal);
		animal.setPersonalmedical(null);

		return animal;
	}

	public Set<Diagnostic> getDiagnostics() {
		return this.diagnostics;
	}

	public void setDiagnostics(Set<Diagnostic> diagnostics) {
		this.diagnostics = diagnostics;
	}

	public Diagnostic addDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().add(diagnostic);
		diagnostic.setPersonalmedical(this);

		return diagnostic;
	}

	public Diagnostic removeDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().remove(diagnostic);
		diagnostic.setPersonalmedical(null);

		return diagnostic;
	}

	public Set<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(Set<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setPersonalmedical(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setPersonalmedical(null);

		return programare;
	}

}
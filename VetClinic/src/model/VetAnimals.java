package model;

import java.util.ArrayList;

/**
 * @author tunde
 *
 * @param <T>
 */



public class VetAnimals<T extends Mamifer> implements Comparable<VetAnimals<T>> {

	private int idAnimal;
	private int age;
	private String animalName;
	private String owner;
	private String race;
	private String weight;
	
	private ArrayList members = new ArrayList<>();
	
	
	/**
	 * @param animal 
	 * @return returns true if animal was added in the list, else returns false if the animal is already int the list
	 */
	public boolean addAnimals(T animal) {
		if(members.contains(animal)) {
			System.out.println("This animal is already in the list");
			return false;
		}
		else {
			members.add(animal);
			System.out.println("Animal added to the list");
			return true;
		}
	}
	
	
	public int getIdAnimal() {
		return idAnimal;
	}



	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}


	/**
	 * @return how many animals are in the array
	 */
	public int numberOfAnimals() {
		return members.size();
	}

	public int getAge() {
		return age;
	}



	/**
	 * @param age of the animal
	 */
	public void setAge(int age) {
		this.age = age;
	}



	public String getAnimalName() {
		return animalName;
	}



	/**
	 * @param animalName name of the animal
	 */
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}



	public String getOwner() {
		return owner;
	}



	/**
	 * @param owner the owner of the animal
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}



	public String getRace() {
		return race;
	}



	/**
	 * @param race
	 */
	public void setRace(String race) {
		this.race = race;
	}



	public String getWeight() {
		return weight;
	}



	/**
	 * @param weight
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}



	public ArrayList getMembers() {
		return members;
	}



	/**
	 * @param members an array of animals
	 */
	public void setMembers(ArrayList members) {
		this.members = members;
	}



	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(VetAnimals<T> animals) {
		if(this.getAge() > animals.getAge())
			return this.getAge();
	}

}

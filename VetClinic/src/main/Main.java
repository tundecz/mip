package main;

import util.DatabaseUtil;

import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Caine;
import model.Diagnostic;
import model.Iepure;
import model.Personalmedical;
import model.Pisica;
import model.Programare;
import model.VetAnimals;

public class Main extends Application{
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		try {
			
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene = new Scene(root, 800, 800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Map<Integer, Animal> animals = new HashMap();

	public static void main(String[] args) throws Exception {
		//Caine bob = new Caine("Bob",12, "Beagle", "Joe", 20);
		//Caine lilo = new Caine("Lilo", 3, "Corgi", "Stefana", 10);
		//Pisica henry = new Pisica("Henry", 2, "Persan", "Emily", 5);
		//Pisica fluff = new Pisica("Fluff", 4, "Siamez", "Jhon", 3);
		//Iepure josh = new Iepure("Josh", 4, "Marele Alb", "Anna", 8);
		//Iepure stefany = new Iepure("Stefany", 1, "Albastru Vienez", "George", 5);
		
		//VetAnimals<Caine> caini = new VetAnimals<>();
		//VetAnimals<Pisica> pisici = new VetAnimals<>();
		//VetAnimals<Iepure> iepuri = new VetAnimals<>();
		
		//addToList addDogs = () -> {VetAnimals<Caine> dogs = new VetAnimals<>();};
		//addToList addCats = () -> {VetAnimals<Pisica> cats = new VetAnimals<>();};
		//addToList addRabbits = () -> {VetAnimals<Iepure> rabbits = new VetAnimals<>();};
		
		
		
		//DatabaseUtil dbUtil = new DatabaseUtil();
		//dbUtil.setUp();
		//dbUtil.startTransaction();
		//dbUtil.commitTransaction();
		//dbUtil.printAllAnimalsFromDB();
		//dbUtil.closeEntityManager();
		
		
		
		
		
		launch(args);
		
		
		
		//animals.put(1, new Animal());
		//animals.put(2, new Animal());
		//animals.put(3, new Animal());
		//animals.put(4, new Animal());
		//animals.put(5, new Animal());
		
		
		//animals.get(1).addToMap();
		//animals.get(2).addToMap();
		//animals.get(3).addToMap();
		//animals.get(4).addToMap();
		//animals.get(5).addToMap();
		
		//LongRunningTask longRunningTask = new LongRunningTask();
		//longRunningTask.setAnimal(new Animal() {
			
			//@Override
			//public void addToMap() {
				//System.out.println("The task has been completed!");
			//}	
		//});
		
		//System.out.println("Starting the long running task!");
		//longRunningTask.run();

		
	}
	
	
	
	
	interface addToList{
		void add();
	}
}

package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


import model.Animal;
import model.Personalmedical;
import model.Programare;
import model.Diagnostic;

public class DatabaseUtil {
	
	public static EntityManagerFactory entityManagrFactory;
	public static EntityManager entityManager;
	
	public void setUp() throws Exception {
		entityManagrFactory = Persistence.createEntityManagerFactory("VetClinic");
		entityManager = entityManagrFactory.createEntityManager();
	}
	
	
	/**
	 * @param animal
	 */
	public void saveAnimal(Animal animal) { 
		entityManager.persist(animal);
	}
	
	/**
	 * @param diagnostic
	 */
	public void saveDiagnostic(Diagnostic diagnostic) { 
		entityManager.persist(diagnostic);
	}
	
	/**
	 * @param personalMedical
	 */
	public void saveMedicaPerson(Personalmedical personalMedical) { 
		entityManager.persist(personalMedical);
	}
	
	/**
	 * @param programare
	 */
	public void saveProgramare(Programare programare) { 
		entityManager.persist(programare);
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() {
		entityManager.close();
	}
	
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from VetClinic.Animal").getResultList();
		for(Animal animal : results) {
			System.out.println("Animal :" + animal.getAnimalName()); //+ "has ID " + animal.getIdanimal() + " has age " + animal.getAge() + ". It`s race is" + animal.getRace() + "and it weights " + animal.getWeight() + ". Owner: " + animal.getOwner());
		}
	}
	
	public void printAllDiagnosticFromDB() {
		List<Diagnostic> results = entityManager.createNativeQuery("Select * from VetClinic.Diagnostic").getResultList();
		for(Diagnostic diagnostic : results) {
			System.out.println("Diagnostic :" + diagnostic.getTitle() + "has ID" +diagnostic.getIdDiagnostic());
		}
	}
	
	public void printAllMedicalPersonFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from VetClinic.Personalmedical").getResultList();
		for(Personalmedical personalMedical :results) {
			System.out.println();
		}
	}
	
	
	public void printAllAppointmenPersonFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from VetClinic.Programare").getResultList();
		for(Programare programare : results) {
			System.out.println();
		}
	}
	
	public List<Animal> animalList(){
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT * FROM Animal a", Animal.class).getResultList();
		return animalList;
	}
	

	
	
	
	
	

}

package controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.xml.crypto.KeySelector.Purpose;

//import com.sun.scenario.animation.AnimationPulseMBean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import model.Animal;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML 
	private ListView<String> listView; 
	
	public void populateMainList() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Animal> animalDBList = (List <Animal>) db.animalList();
		ObservableList<String> animalNamesList = getAnimalName(animalDBList);
		 listView.setItems(animalNamesList);
		 listView.refresh();
		 db.closeEntityManager(); 
	}
	
	public ObservableList<String> getAnimalName (List<Animal> animals){
		ObservableList<String> names  = FXCollections.observableArrayList();
		for(Animal a: animals) {
			names.add(a.getAnimalName());
		}
		return names;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainList();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

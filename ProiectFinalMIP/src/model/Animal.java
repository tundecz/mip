package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idanimal;

	private int age;

	private String animalName;

	private String owner;

	private String race;

	private int weight;

	//bi-directional many-to-one association to Diagnostic
	@OneToMany(mappedBy="animal")
	private Set<Diagnostic> diagnostics;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idDoctor")
	private Personalmedical personalmedical;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="animal")
	private Set<Programare> programares;

	public Animal() {
	}

	public int getIdanimal() {
		return this.idanimal;
	}

	/**
	 * @param idanimal id-ul unic al animalului
	 */
	public void setIdanimal(int idanimal) {
		this.idanimal = idanimal;
	}

	public int getAge() {
		return this.age;
	}

	/**
	 * @param age varsta animalului
	 */
	public void setAge(int age) {
		this.age = age;
	}

	public String getAnimalName() {
		return this.animalName;
	}

	/**
	 * @param animalName numelele animalului
	 */
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getOwner() {
		return this.owner;
	}

	/**
	 * @param owner stapanul animalului
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getRace() {
		return this.race;
	}

	/**
	 * @param race rasa animalului
	 */
	public void setRace(String race) {
		this.race = race;
	}

	public int getWeight() {
		return this.weight;
	}

	/**
	 * @param weight setarea greutatii animalului
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Set<Diagnostic> getDiagnostics() {
		return this.diagnostics;
	}

	/**
	 * @param diagnostics vector de diagnostic -: fiecare animal poate avea mai multe diagnostice
	 */
	public void setDiagnostics(Set<Diagnostic> diagnostics) {
		this.diagnostics = diagnostics;
	}

	/**
	 * @param diagnostic 
	 * @return diagnostoc
	 */
	public Diagnostic addDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().add(diagnostic);
		diagnostic.setAnimal(this);

		return diagnostic;
	}

	/**
	 * @param diagnostic stergerea diagnosticului
	 * @return
	 */
	public Diagnostic removeDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().remove(diagnostic);
		diagnostic.setAnimal(null);

		return diagnostic;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	/**
	 * @param personalmedical setare personal medical
	 */
	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

	public Set<Programare> getProgramares() {
		return this.programares;
	}

	/**
	 * @param programares vector de programari
	 */
	public void setProgramares(Set<Programare> programares) {
		this.programares = programares;
	}

	/**
	 * @param programare adaugare programari
	 * @return
	 */
	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	/**
	 * @param programare stergere programari
	 * @return
	 */
	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

}
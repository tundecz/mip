package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramare;

	@Temporal(TemporalType.DATE)
	private Date date;

	private int hour;

	private int idDoctor;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	public Programare() {
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public Date getDate() {
		return this.date;
	}

	/**
	 * @param date data programarii
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	public int getHour() {
		return this.hour;
	}

	/**
	 * @param hour ora programarii
	 */
	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getIdDoctor() {
		return this.idDoctor;
	}

	/**
	 * @param idDoctor identificatorul unic al personalului medical
	 */
	public void setIdDoctor(int idDoctor) {
		this.idDoctor = idDoctor;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	/**
	 * @param animal animalul care are programare
	 */
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

}
package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the diagnostic database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnostic.findAll", query="SELECT d FROM Diagnostic d")
public class Diagnostic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idDiagnostic;

	@Temporal(TemporalType.DATE)
	private Date date;

	private String description;

	private int idDoctor;

	private String title;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	public Diagnostic() {
	}

	public int getIdDiagnostic() {
		return this.idDiagnostic;
	}

	/**
	 * @param idDiagnostic id unic diagnostic
	 */
	public void setIdDiagnostic(int idDiagnostic) {
		this.idDiagnostic = idDiagnostic;
	}

	public Date getDate() {
		return this.date;
	}

	/**
	 * @param date data diagnosticului
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description descriere diagnostic
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public int getIdDoctor() {
		return this.idDoctor;
	}

	/**
	 * @param idDoctor id unic personal medical
	 */
	public void setIdDoctor(int idDoctor) {
		this.idDoctor = idDoctor;
	}

	public String getTitle() {
		return this.title;
	}

	/**
	 * @param title nume diagnostic
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

}
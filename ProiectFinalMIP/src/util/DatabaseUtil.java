package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Diagnostic;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {

	
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("ProiectFinalMIP");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}

	public void saveDiagnostic(Diagnostic diagnostic) {
		entityManager.persist(diagnostic);
	}

	public void savePersonalMedical(Personalmedical personalMedical) {
		entityManager.persist(personalMedical);
	}

	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void closeEntityManager() {
		entityManager.close();
	}

	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from cabinetveterinar.animal",Animal.class).getResultList();

		for (Animal animal : results) {
			System.out.println("Animal" + animal.getAnimalName() + "has ID" + animal.getIdanimal());
		}
	}

	public void printAllDiagnosicsFromDB() {
		List<Diagnostic> results = entityManager.createNativeQuery("Select * from cabinetveterinar.diagnostic",Diagnostic.class)
				.getResultList();

		for (Diagnostic diagnostic : results) {
			System.out.println("Diagnosic" + diagnostic.getTitle() + "has ID" + diagnostic.getIdDiagnostic());
		}
	}

	public void printAllDoctorsFromDB() {
		List<Personalmedical> results = entityManager
				.createNativeQuery("Select * from cabinetveterinar.personalmedical",Personalmedical.class).getResultList();

		for (Personalmedical personalMedical : results) {
			System.out.println(
					"Personal medical" + personalMedical.getName() + "has ID" + personalMedical.getIdPersonalMedical());
		}
	}

	public void printAllAppointmentsFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from cabinetveterinar.animal",Programare.class)
				.getResultList();

		for (Programare programare : results) {
			System.out.println("Programare" + programare.getDate() + "has ID" + programare.getIdProgramare());
		}
	}
	
	public List<Animal> animalList(){
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a from Animal a",Animal.class).getResultList();
		return animalList;
	}
	
	public List<Diagnostic> diagnosticList(){
		List<Diagnostic> diagnosticList = (List<Diagnostic>)entityManager.createQuery("SELECT d from Diagnostic d",Diagnostic.class).getResultList();
		return diagnosticList;
	}
}

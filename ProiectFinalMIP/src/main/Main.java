package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Personalmedical;
import util.DatabaseUtil;

public class Main extends Application{

	public static void main(String[] args) throws Exception {
		/*DatabaseUtil dbUtil = new DatabaseUtil();
		Personalmedical personalMedical = new Personalmedical();
		personalMedical.setName("Tunde");
		personalMedical.setIdPersonalMedical(5);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.savePersonalMedical(personalMedical);
		dbUtil.commitTransaction();
		dbUtil.printAllDoctorsFromDB();
		dbUtil.closeEntityManager();*/
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene = new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
